<?php

namespace App\Http\Controllers;

use Request;
use App\Models\Category;
use App\Models\Announcement;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;

class PublicController extends Controller
{


    public function index()
    {
        // $announcements=Announcement::all()->take(5);
        $announcements=Announcement::orderBy('created_at','desc')->where('is_accepted','<>',0)->take(5)->get();

        return view('home', compact('announcements'));    
    }




    public function announcementsByCategory ($name, $category_id)
    {
        $category = Category::find($category_id);
        $announcements = $category->announcements()->orderBy('created_at', 'desc')->paginate(5);
        return view('announcements.announcements', compact('category', 'announcements'));      
    }

    public function locale($locale)
    {
        session()->put('locale', $locale);       
        return redirect()->back();
    }
    
}
