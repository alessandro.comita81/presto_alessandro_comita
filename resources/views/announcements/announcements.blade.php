<x-layout>
    
    <div class="container-fluid p-0">
        <div class="row m-0 mt-5 p-0 justify-content-center">
            <div class="col-12 col-md-10  m-0 p-md-0 ps-2 d-flex">
                    <h2 class="titolo2 charcoal">Annunci per {{ $category->name }} </h2>
            </div>        


                
            {{-- contenitore cards --}}
    
            <div class="row m-0 mt-1 p-0 justify-content-center pt-2">
          
                @foreach ($announcements as $announcement)
                    <x-card :announcement='$announcement' />
                @endforeach    
            </div>


                    {{-- {{ $announcements->links() }} --}}

            
                    <div class="d-flex justify-content-center mt-3 p-0" >
                        {!! $announcements->links() !!}
                    </div>


        </div>
    </div>
    
    
    


                {{-- paginazione --}}
                {{-- <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-md-8">
                            {{ $announcements->links() }}
                        </div>
                    </div>
                </div>  --}}


</x-layout>