<x-layout>







<div class="container-fluid">
    <div class="row justify-content-center bg-persian-green  ">
        <div class="col-12 col-md-8   ">
            <h1 class="titolo1 text-center text-white">{{ __('ui.welcome') }}</h1>
        </div>      
    </div>  

    <script>

    </script>
        

            @if (session('announcement.created.success'))
            <div id="avviso" class="d-flex  row w-100 p-0 mt-2 justify-content-center align-items-center   position-absolute">
                    <div class="d-flex align-items-center justify-content-center rounded  w-25 h-100 bg-sandy-brown text-center  charcoal    ">

                      Annuncio creato correttamente
                    </div>
            </div>
            @endif
        
        
            @if (session('status'))
            <div class="row justify-content-center py-2">
                <div class="col-12 col-md-8 alert alert-success w-100" role="alert">
                    {{ session('status') }}
                </div>
            </div>
            @endif
 


            {{-- messaggio che avvisa l'utente che non ha privilegi sufficienti  --}}

            @if (session('access.denied.revisor.only'))
            <div id="avviso" class=" row  p-0 mt-2 w-100 px-md-5 justify-content-center align-items-center   position-absolute">
                    <div class="col-12 col-md-4 d-flex align-items-center justify-content-center rounded   h-100 bg-sandy-brown text-center  charcoal    ">

                        Accesso non consentito.<br>Servono i privilegi da revisore.
                    </div>
            </div>
            @endif






    <div class="row m-0 mt-3 p-0 justify-content-center">
        <div class="col-12 col-md-10  m-0 p-0 d-flex">
                <h2 class="titolo2 charcoal">
                    
                    {{ __('ui.Last_announcements')}}

                </h2>
        </div>        
    </div>
    
    
        {{-- contenitore cards --}}

        <div class="row m-0 mt-1 p-0 justify-content-center pt-2">
            <div class="col-12 d-flex flex-wrap justify-content-center  pb-3 pb-md-0 px-0 px-md-2 mx-0">
                @foreach ($announcements as $announcement)
                    <x-card :announcement='$announcement' />
                @endforeach  
            </div>  
        </div>

        





</div>
</x-layout>
