<x-layout>
    
    
    <div class=" container ">
        <div class=" p-0 row d-flex justify-content-center"> 
            <div class=" col-12 col-md-6  p-0 m-0 ">
                
                <div class="container box-revisione  bg-maize-crayola pb-4 p-0 mb-3">
                    <div class="row m-0 mt-1 p-0 justify-content-center mt-5 pt-1">
                        <div class="col-12 d-flex flex-wrap justify-content-center  pt-4  px-0  mx-0">
                            
                            <p class="d-flex align-items-end p-0 m-0 paragrafo-annuncio-numero">
                            
                            
                            
                            
                                @if ($announcement)
                                    Annuncio numero {{ $announcement->id }}     
                                @else
                                Non ci sono annunci da revisionare
                                @endif  
                             </p>
                            
                        </div>  
                    </div>
                    
                    @if ($announcement)
                        <div class="row m-0  mt-1 p-0 justify-content-center pt-0">
                            <div class="col-12 d-flex flex-wrap justify-content-center  py-2 px-0  mx-0">
                                
                                <x-card :announcement='$announcement' />
                                
                            </div>  
                        </div>
                    
                
                    
                        <div class="row  m-0 mt-1 p-0 justify-content-center align-items-center" >



                      
                       
                            <div class="col-6  p-0 d-flex justify-content-end pe-3">
                                <form action="{{ route('revisor.reject',
                                $announcement->id) }}" method="POST">
                                    @csrf
                                    <button type="submit"
                                        class="btn bottone-risposta  d-flex justify-content-center align-items-center  bg-burnt-sienna">
                                    
                                        <i class="bi bi-x-circle icona-risposta text-white" ></i>
                                    </button>
                                </form>
                            </div>
                       
                      
                     
                        <div class="col-6  p-0 d-flex justify-content-start ps-3">
                            <form action="{{ route('revisor.accept',
                            $announcement->id) }}" method="POST">
                                @csrf
                                <button type="submit"
                                    class="btn bottone-risposta d-flex justify-content-center align-items-center bg-persian-green p-0">

                                <i class="bi bi-check-circle icona-risposta text-white"></i>
                                </button>
                            </form>
                       
                        
                        </div>
                    @endif
                    
                </div>
                
                
                
            </div>
        </div>
        
    </div> 
        {{-- {{  dd($announcements->toArray()) }} --}}
        
        
        
        
        
        
    </x-layout>