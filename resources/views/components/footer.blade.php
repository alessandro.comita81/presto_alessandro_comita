

     

<footer  class="footer  bg-charcoal  w-100 ">
    {{-- <div class="b-example-divider w-100"></div>     --}}
    <div class="container">
      <div class="row py-3">
        
        <div class="col-12 col-md-8 d-flex justify-content-md-start justify-content-center mb-md-0 mb-2 align-items-center">

            <span class="text-white m-0 p-0 ">&copy; 2022 Presto Company, Inc</span>
          </div>
        
          <div class="col-12 col-md-4 d-flex align-items-center justify-content-md-end justify-content-center">
            <a class="text-white ms-3" href="#"><i class="icona-bootstrap bi bi-twitter"></i></a>

            <a class="text-white ms-3" href="#"><i class="icona-bootstrap bi bi-instagram"></i></a>

            <a class="text-white ms-3" href="#"><i class="icona-bootstrap bi bi-facebook"></i></a>
          </div>
        </div>



    </div>
  </footer>