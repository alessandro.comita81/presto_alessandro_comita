
    @if ($announcement)
        <div class="container position-relative carta  mb-2 mx-2">
            <div class="row justify-content-center">
                <div class="cornice-immagine-annuncio">
                    {{-- <img class="immagine-annuncio  p-0" 
                    src="{{ Storage::url($announcement->images->first()->file) }}"> --}}
                    <img class="immagine-annuncio p-0"
                        src="{{ $announcement->images->first()->getUrl(300, 200) }}">

                </div>
                <div class="position-absolute top-50 end-0 translate-middle-y text-end p-0  ">
                    <a class=""
                        href="{{ route('public.announcement.category', 
                            [$announcement->category->name, $announcement->category->id])   }}">



                                <span class=" mt-2 bg-burnt-sienna text-white  px-2 py-1 rounded-start link-category">
                                            
                                    @if (App::currentLocale()=='it')
                                        {{ $announcement->category->nome_it }} 
                                    @elseif (App::currentLocale()=='es')
                                        {{ $announcement->category->nome_es }} 
                                    @elseif (App::currentLocale()=='en')
                                        {{ $announcement->category->name }} 
                                    @else
                                        {{ $announcement->category->name }} 
                                    @endif
                                
                                </span>
                    </a>            
                </div>
                
                
                
                <h5 class="mt-2 charcoal ms-3">{{ $announcement->title }} </h5>
                <p class="ms-3">{{ $announcement->body }}</p>
                <div class="footer-card p-0 p-2 d-flex justify-content-between position-absolute bottom-0">
                    <div class=""><h6 class="m-0">{{ $announcement->user->name }}</h6></div>
                        <div class="d-flex justify-content-end">
                            <div class="d-flex align-items-end">
                                <h6 class="testo-data-ora m-0">{{ $announcement->created_at->format('H:i:s') }} | </h6></div>
                            <div class=" d-flex align-items-end">
                                <h6 class="testo-data-ora m-0 ms-1"> {{ $announcement->created_at->format('d/m/y') }}</h6></div>
                        </div>
                </div>
            </div>
        </div>
@endif