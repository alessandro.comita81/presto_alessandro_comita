<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    


    

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>PrestoX</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
   



    {{-- favicon --}}
    {{-- <link rel="apple-touch-icon" sizes="180x180" href="apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="favicon-16x16.png"> --}}
    {{-- <link rel="icon" href="{{ URL::asset('public/images/favicon-32x32.png') }}" type="image/x-icon"/> --}}

    <link rel="shortcut icon" href="{{ asset('images/favicon-32x32.png') }}"  type='image/x-icon'>


    {{-- <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png"> --}}

    {{-- <link rel="shortcut icon" href="{{ asset('public/images/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" href="{{ asset('public/images/favicon-32x32.png') }}"> --}}


    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>



    <div id="page-container" >
        <div id="content-wrap">

            <x-navbar />
     
            {{ $slot }}
        
          </div>

        <div id="footer">
            <x-footer />
        </div>

    </div>





</body>
</html>
