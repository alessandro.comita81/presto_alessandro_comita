if (document.getElementById("avviso"))
{
  const eAvviso = document.getElementById("avviso");



  // ritardo prima della comparsa
  const myTimeout0 = setTimeout(appear, 300);
          
  function appear() {
    eAvviso.classList.add("compari");
  }
  
  // il box del messaggio della creazione dell'annuncio sparisce con una dissolvenza
  const myTimeout = setTimeout(disappear, 4000);
          
  function disappear() {
    eAvviso.classList.remove("compari");
    eAvviso.classList.add("sparisci");
  }
  
  // il box del messaggio della creazione dell'annuncio viene eliminato fisicamente dal flusso della pagina
  const myTimeout2 = setTimeout(destroy, 5000);
  
  function destroy() {
    eAvviso.classList.add("d-none");
  }
  
}


