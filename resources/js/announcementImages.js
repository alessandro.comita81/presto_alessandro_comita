// se quando ha finito di caricare la finestra, esiste l'elemento "drophere", allora esegui lo script
    
    window.onload = 
    function()
    {
        if (document.getElementById('drophere'))
        {
            // vado a catturare il token che laravel genera e inserisce nella sezione meta
            let csrfToken = $('meta[name="csrf-token"]').attr('content');


            let uniqueSecret = $('input[name="uniqueSecret"]').attr('value');

            let myDropzone = new Dropzone
            ('#drophere', 
                { url: '/announcement/images/upload',
            
        
                    params:
                    {
                        // ogni volta che l'utente inserisce una immagine
                        // Dropzone fa una chiamata POST verso la rotta 
                        // con l'immagine (l'url sopraindicato),
                        // e invia anche questo token
                        // per superare la guardia di sicurezza di Laravel
                        _token: csrfToken,

                        // inoltre invia un secondo elemento
                        // che identifica la form
                        // al server viene così inviata sia l'immagine,
                        // sia il "segreto"                    
                        uniqueSecret: uniqueSecret
                    },

                    // quando l'utente aggiunge una immagine
                    // metti un link per fare il remove
                    addRemoveLinks: true,

                    init: function()
                    {
                        $.ajax
                        ({
                            type: 'GET',
                            url: '/announcement/images',
                            data:
                            {
                                uniqueSecret: uniqueSecret
                            },
                            dataType: 'json'
                        }).done(function(data)
                            {
                                $.each(data, function(key, value)
                                {
                                    let file = 
                                    {
                                        serverId: value.id
                                    };

                                    myDropzone.options.addedfile.call(myDropzone, file);
                                    myDropzone.options.thumbnail.call(myDropzone, file, value.src);

                                });
                            });
                    }
                });




            myDropzone.on("success", function(file, response)
            {
                // prendiamo la risposta del server e ne catturiamo l'ID.
                // L'ID viene memorizzzato all'interno dell'oggetto file di Dropzone
                file.serverId = response.id;
            });

            myDropzone.on("removedfile", function(file)
            {
                    $.ajax
                    ({
                        type: 'DELETE',
                        url: '/announcement/images/remove',
                        data: 
                        {
                            _token: csrfToken,
                            id: file.serverId,
                            uniqueSecret: uniqueSecret
                        },
                        dataType: 'json'
                    });
                });
                

            
        }
    }

